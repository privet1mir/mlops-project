models = ["rf", "lr"]
preproces = ["first", "second"]

rule all: 
    input: 
        expand("docs/report.{type}.{preproc_num}.json", type=models, preproc_num=preproces),

rule preprocessing_first:
    input:
        "mushroom_cleaned.csv"
    output:
        train_data="datasets/train.first.csv",
        test_data="datasets/test.first.csv"
    params: 
        test_size=0.25
    shell: 
        "python src/preprocessing_first.py {input} {output} {params}" 

rule preprocessing_second:
    input:
        "mushroom_cleaned.csv"
    output:
        train_data="datasets/train.second.csv",
        test_data="datasets/test.second.csv"
    params: 
        test_size=0.25
    shell: 
        "python src/preprocessing_second.py {input} {output} {params}" 

rule training:
    input:
        "datasets/train.{preproc_num}.csv"
    output: 
        "models/{type}.{preproc_num}.joblib"
    threads: 
        workflow.cores * 0.5
    params: 
        n_estimators=50,
        max_depth=10,
        C=0.01
    shell: 
        "python src/training.py --threads {threads} {input} {output} {params} {wildcards.type}"

rule validating: 
    input: 
        "models/{type}.{preproc_num}.joblib",
        "datasets/test.{preproc_num}.csv"
    output: 
        "docs/report.{type}.{preproc_num}.json"
    shell: 
        "python src/validating.py {input} {output} {wildcards.type}"
