## Content

- [Setting up the environment](#setting-up-the-environment)

- [Snakemake pipelines](#snakemake-pipelines)

- [Validation metrics](#validation-metrics)

## Setting up the environment

```
# if you are not in the /snakemake folder use: 

cd snakemake 
```

To reproduce my results firstly you need to build an image from a Dockerfile using: 
```
docker build -t snakemake .
```

Then you need to create and run a new container from the snakemake image: 
```
# we use shell

docker run -it snakemake sh
```

After this to run a snakemake pipeline you need to execute in a shell (**k** means the number of cores used for pipeline): 
```
snakemake --cores k
```

All artifacts (which include 4 model with two different preprocessing and validation metrics) you can see in an container. If you want to see the dag of jobs you can see it predownload at docs/ or create your own by executing: 

```
snakemake --dag | dot -Tpng > docs/dag.png
```

## Snakemake pipelines

![dag graph](./docs/dag.png)

Pipelines include the following steps: 

* Two type of data ***preprocessing***: 

    1. Delete rows with missing values and delete one of the highly correlated features

    2. Delete rows with missing values and scale features in a range between zero and one

* Then you can see ***training*** stage that finalizes in 4 different models. As a baseline models for binary classification task I used logistic regression and random forest classifier. So the artifacts of this stage are: 

    1. Logistic regression trained based on first type preprocessing

    2. Logistic regression trained based on second type preprocessing

    3. Random forest classifier trained based on first type preprocessing

    4. Random forest classifier trained based on second type preprocessing

* Finally - ***validation*** stage, where trained models used for metrics calculation based on its predictions on test data

## Validation metrics

To solve binary classification task I used two models: 

* RandomForestClassifier (n_estimators=50, max_depth=10)

* LogisticRegression(C=0.01)

Validation metrics are tho following: 

| Model & Preprocessing type | Accuracy | Roc auc | f1 | 
| ------- | --- |--- | --- |
| RF_first | 0.95 | 0.95 | 0.96 |
| RF_second | 0.96 | 0.96| 0.96 |
| LR_first | 0.56 | 0.58 | 0.46 |
| LR_second | 0.55 | 0.5 | 0.71 |