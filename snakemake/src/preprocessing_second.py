import sys
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

def preprocess(input: str, train_out: str, test_out: str, test_size: float, random_seed: int = 19) -> None: 
    """ Preprocess data in the following ways: 
        1. Delete rows with missing values
        2. Scale features in a range between zero and one

    Args:
        input (str): path to data.csv file
        train_out (str): path to save train dataset
        test_out (str): path to save test dataset
        test_size (float): control proportion between train and test data volumes
        random_seed (int): fixes random number generator for reproducibility
    """
    data = pd.read_csv(input)
    # drop missing values 
    data.dropna(inplace=True)
    # scale features
    scaler = MinMaxScaler()
    columns = data.columns
    data[[column for column in columns[:-1]]] = scaler.fit_transform(data[[column for column in columns[:-1]]])
    # split data into train and test
    train_data, test_data = train_test_split(data, test_size=test_size, shuffle=True, random_state=random_seed)
    # reset indexes to start from 0
    train_data.reset_index(drop=True, inplace=True)
    test_data.reset_index(drop=True, inplace=True)  

    # save new dataframes
    train_data.to_csv(train_out)
    test_data.to_csv(test_out)

if __name__ == "__main__": 

    input = sys.argv[1]
    train_out = sys.argv[2]
    test_out = sys.argv[3]
    test_size = float(sys.argv[4])

    preprocess(input, train_out, test_out, test_size)
    