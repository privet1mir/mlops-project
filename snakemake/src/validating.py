import sys
import json
from sklearn.metrics import accuracy_score, roc_auc_score, f1_score
from joblib import load
import pandas as pd

def validation(model_path: str, test_data_path: str, report: str, model_type: str) -> None: 
    """ Loads model and validate on test data to obtain classification metrics

    Args:
        model_path (str): path to model
        test_data_path (str): path to test.csv file
        report (str): path to save report.json file with validation metrics for the model
        model_type (str): type of the classifier (rf or lr)
    """
    test_data = pd.read_csv(test_data_path)
    X_test, y_test = test_data.drop(columns=["class"]), test_data[["class"]].values.ravel()
    model = load(model_path)
    predictions = model.predict(X_test)
    metrics = {
        "accuracy": accuracy_score(predictions, y_test),
        "roc_auc": roc_auc_score(y_test, predictions),
        "f1": f1_score(predictions, y_test)
    }

    with open(report, 'w') as outfile: 
        json.dump(metrics, outfile)


if __name__ == "__main__": 
    model_path = sys.argv[1]
    test_data_path = sys.argv[2]
    report = sys.argv[3]
    model_type = sys.argv[4]
    validation(model_path, test_data_path, report, model_type)