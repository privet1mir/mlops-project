import sys
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from joblib import dump

def training(input: str, output: str, n_estimators: int, max_depth: int, const: float, model_type: str, random_seed: int = 19) -> None: 
    """ Train classifier on train data and save the model

    Args:
        input (str): path to train.csv file
        output (str): path to save model
        n_estimators (int): rf parameter - number of trees in the forest
        max_depth (int): rf parameter - the maximum depth of the tree
        const (float): lr parameter - inverse of regularization strength
        model_type (str): type of the classifier (rf or lr)
        random_seed (int): fixes random number generator for reproducibility
    """
    train_data = pd.read_csv(input)
    # devide data into features and labels
    X_train, y_train = train_data.drop(columns=["class"]), train_data[["class"]].values.ravel()
    # train classifier
    if model_type == "rf": 
        classifier = RandomForestClassifier(n_estimators=n_estimators, max_depth=max_depth, random_state=random_seed).fit(X_train, y_train)
    else: 
        classifier = LogisticRegression(C=const, random_state=random_seed).fit(X_train, y_train)
        
    dump(classifier, output) 

if __name__ == "__main__": 
    # first and second arg used for threads -> see Snakefile
    input = sys.argv[3]
    output = sys.argv[4]
    n_estimators = int(sys.argv[5])
    max_depth = int(sys.argv[6])
    const = float(sys.argv[7])
    model_type = sys.argv[8]
    training(input, output, n_estimators, max_depth, const, model_type)
    