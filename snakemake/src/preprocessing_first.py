import sys
import pandas as pd
from sklearn.model_selection import train_test_split

def preprocess(input: str, train_out: str, test_out: str, test_size: float, random_seed: int = 19) -> None: 
    """ Preprocess data in the following ways: 
        1. Delete rows with missing values
        2. Delete one of the highly correlated features

    Args:
        input (str): path to data.csv file
        train_out (str): path to save train dataset
        test_out (str): path to save test dataset
        test_size (float): control proportion between train and test data volumes
        random_seed (int): fixes random number generator for reproducibility
    """
    data = pd.read_csv(input)
    # drop missing values 
    data.dropna(inplace=True)
    # drop features with high correlation (> than 0.8)
    columns_to_delete = []
    for i in range(len(data.columns)): 
        for j in range(i, len(data.columns)): 
            if (data.columns[i] != data.columns[j]) and (data.corr()[data.columns[i]][data.columns[j]] > 0.8): 
                if (data.columns[i] not in columns_to_delete) and data.columns[i] != "class":
                        columns_to_delete.append(data.columns[i])

    data.drop(columns=columns_to_delete, inplace=True)
    # split data into train and test
    train_data, test_data = train_test_split(data, test_size=test_size, shuffle=True, random_state=random_seed)
    # reset indexes to start from 0
    train_data.reset_index(drop=True, inplace=True)
    test_data.reset_index(drop=True, inplace=True)  

    # save new dataframes
    train_data.to_csv(train_out)
    test_data.to_csv(test_out)

if __name__ == "__main__": 

    input = sys.argv[1]
    train_out = sys.argv[2]
    test_out = sys.argv[3]
    test_size = float(sys.argv[4])

    preprocess(input, train_out, test_out, test_size)
    