FROM python:3.12

# Copy dependencies
WORKDIR /app
COPY pyproject.toml ./

# Install poetry
RUN pip install poetry

RUN poetry config virtualenvs.create false  \
    && poetry install \
    && poetry run kaggle datasets download -d new-york-city/ny-2015-street-tree-census-tree-data \
    && unzip ny-2015-street-tree-census-tree-data.zip -d . \
    && rm ny-2015-street-tree-census-tree-data.zip 

COPY . /app 
