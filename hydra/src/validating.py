import hydra
import json
from joblib import load
import logging
import pandas as pd
from hydra.utils import instantiate
from sklearn.metrics import accuracy_score, roc_auc_score, f1_score
from omegaconf import DictConfig

log = logging.getLogger(__name__)

@hydra.main(config_path='../conf', config_name='config')
def validate(cfg: DictConfig): 
    log.info("Load model")
    training = instantiate(cfg.training)
    validating = instantiate(cfg.validating)
    test_data = pd.read_csv(training.path_to_test)
    X_test, y_test = test_data.drop(columns=["class"]), test_data[["class"]].values.ravel()
    model = load(training.path_to_model)
    log.info("Start validating")
    predictions = model.predict(X_test)
    metrics = {
        "accuracy": accuracy_score(predictions, y_test),
        "roc_auc": roc_auc_score(y_test, predictions),
        "f1": f1_score(predictions, y_test)
    }
    with open(validating.path_to_report, 'w') as outfile: 
        json.dump(metrics, outfile)
        
    log.info("Save validation metrics to /docs")

if __name__ == "__main__": 
    validate()