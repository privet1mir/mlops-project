import hydra
import logging
import pandas as pd
from hydra.utils import instantiate
from omegaconf import DictConfig
from joblib import dump

log = logging.getLogger(__name__)

@hydra.main(config_path='../conf', config_name='config')
def train(cfg: DictConfig):
    log.info("Upload data and start training")
    model = instantiate(cfg.model)
    training = instantiate(cfg.training)
    train_data = pd.read_csv(training.path_to_train)
    X_train, y_train = train_data.drop(columns=["class"]), train_data[["class"]].values.ravel()

    model.fit(X_train, y_train)
    log.info("Training complete!")

    dump(model, training.path_to_model)

    log.info(f"Model saved to {training.path_to_model}")


if __name__=='__main__':
    train()