import hydra
import logging
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from hydra.utils import instantiate
from omegaconf import DictConfig

log = logging.getLogger(__name__)

@hydra.main(config_path='../conf', config_name='config')
def preprocess(cfg: DictConfig): 
    log.info("Upload data and start preprocessing")
    preprocessing = instantiate(cfg.preprocessing)
    data = pd.read_csv(preprocessing.dataset)
    # drop missing values 
    data.dropna(inplace=True)
    # scale features
    scaler = MinMaxScaler()
    columns = data.columns
    data[[column for column in columns[:-1]]] = scaler.fit_transform(data[[column for column in columns[:-1]]])
    # split data into train and test
    train_data, test_data = train_test_split(data, test_size=preprocessing.test_size, shuffle=True, random_state=preprocessing.random_state)
    # reset indexes to start from 0
    train_data.reset_index(drop=True, inplace=True)
    test_data.reset_index(drop=True, inplace=True)  
    # save new dataframes
    train_data.to_csv(preprocessing.train_data)
    test_data.to_csv(preprocessing.test_data)
    log.info("Data preprocessed and saved")

if __name__ == "__main__": 
    preprocess()